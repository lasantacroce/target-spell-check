# Target Spell Check (MATLAB function)
Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)

## Description
This function allows users to quickly grade incorrect spellings.

## Use         
Use this if you don't want to count spelling errors as incorrect, but also don't want to grade each trial's response for each participant one at a time.

## Interface
- Input: A folder with your data files and a .csv file with your target words
- Task: Unique incorrect responses will display one at a time, you will select which target (if any) they were trying to write, and you will never see that incorrect word ever again every time you run the function
- Output: A .mat file with all spelling errors for each target word and a .mat file for each data file including a correct/incorrect column
- Tip: Ask participants not to "key smash" (type in random letters) if they are unsure of the answer
- Download example data folders to see how files are set up or to see the function in action!

## Instructions:
1. Dowload all files in the 'DOWNLOADME' folder
2. Add that function folder to MATLAB path
3. Add data files to a single folder 
    - Data files must be .csv files or .mat files
    - If .csv files, they must all have a column called "responses" with the participants' responses and a column called "targets" with the targets for each trial
    - If .mat files, the data must all be in a table called "data" with the same variables mentioned above
    - Include another .csv file with all of the target words (without paddings) in a single column without a column header
    - Do not include any other .mat or .csv files in that folder (a .mat file will appear with the spellings, so don't delete that)
4. Type "targetSpellCheck" into the MATLAB Command Window
5. A box with reminder file instructions will appear-- click to continue to file settings
6. In the file settings box, select the folder that contains your data, indicate if your files are .csv or .mat, select the .csv file with your 7arget words, and click 'Save Settings'
8. A list dialogue box will pop up with all of your target words and a participant's incorrect word
9. Select which word they were trying to spell or 'none' if they entered an entirely different word, and click 'Enter'
10. Do this for each incorrect spelling (you will never see the words twice, so there will be fewer words every time you run it)
11. A new folder will appear within your data folder with new .mat files that include a table called "dataTable", which is a copy of your original "data" table, but now with a "correct" column with 1 for a correct answer and 0 for an incorrect answer for each trial

## Visuals
![](Examples/targetSpellCheck_gif.gif)

## Support
If you have any questions/errors/comments/ideas for additions, contact me at lindsayas22@gmail.com and include "Target Spell Check" in the subject line
