%% Going through incorrect responses and checking for spelling errors
    % Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com), 2022
    % Description: This function allows users to quickly grade incorrect spellings. 
    % Use: Use this if you don't want to count spelling errors as incorrect, but also don't want to grade each response one at a time.
    % Interface: 
        % Input: A folder with your data files and a .csv file with your target words
        % Task: Unique incorrect responses will display one at a time, you will select which target (if any) they were trying to write, and you will never see that incorrect word ever again every time you run the function
        % Output: A .mat file with all spelling errors for each target word and a .mat file for each data file including a correct/incorrect column
        % Tip: Ask participants not to "key smash" (type in random letters) if they are unsure of the answer
    % Instructions:
        % 1. Add function folder to path
        % 2. Add data files to a single folder 
            % Data files must be .csv files or .mat files
            % If .csv files, they must all have a column called "responses" with the participants' responses and a column called "targets" with the targets for each trial
            % If .mat files, the data must all be in a table called "data" with the same variables mentioned above
            % Include another .csv file with all of the target words (without paddings) in a single column without a column header
            % Do not include any other .mat or .csv files in that folder (a .mat file will appear with the spellings, so don't delete that)
        % 3. Type "targetSpellCheck" into the MATLAB Command Window
        % 4. A box with reminder file instructions will appear-- click to continue to file settings
        % 5. In the file settings box, select the folder that contains your data, indicate if your files are .csv or .mat, select the .csv file with your target words, and click 'Save Settings'
        % 6. A list dialogue box will pop up with all of your target words and a participant's incorrect word
        % 7. Select which word they were trying to spell or 'none' if they entered an entirely different word, and click 'Enter'
        % 8. Do this for each incorrect spelling (you will never see the words twice, so there will be fewer words every time you run it)
        % 9. A new folder will appear within your data folder with new .mat files that include a table called "dataTable", which is a copy of your original "data" table, but now with a "correct" column with 1 for a correct answer and 0 for an incorrect answer for each trial

function targetSpellCheck

%instructions box function
userInstructions;
%data selection function
[dataFolder, targetFile, dataFileType] = filesBox;

%file setup
saveFileName = [dataFolder, '\targetSpellings.mat'];
cd(dataFolder);
filelist = dir(dataFolder);
for i = numel(filelist):-1:1
    filename = [filelist(i).folder, '\', filelist(i).name];
    if filename == string(targetFile) || filename == string(saveFileName) || contains(filelist(i).name, dataFileType) == 0
        filelist(i) = [];
    end
end

%% Setting up correct fruit answers (only done first time)

%try to load file (will exist if function has already run before)
try
    load(saveFileName); 
%if not, get correct answers from first participant's file
catch    
    %targets
    targetTable = readtable(targetFile, 'ReadVariableNames', false);
    targets = lower(targetTable.Variables);
    for i = 1:numel(targets)
        %structure to put all of the "correct" target spellings in
        ct = lower(string(targets(i)));
        targetSpellings.(ct) = ct;
        %making a list of spellings seen before, starting with the targets
        seenBefore(i,1) = ct;
        %setting up all spellings seen array
        allSpellings(i,1) = ct;
    end
    %saving file for future
    save(saveFileName, 'targetSpellings', 'allSpellings', 'seenBefore');
end

%% Load current participants and adding their responses to matrix
for s = 1:numel(filelist)   
    %get table from csv or mat file
    if dataFileType == ".csv"
        %reading in csv
        dataTable = readtable(filelist(s).name);
    elseif dataFileType == ".mat"
        load(filelist(s).name);
        dataTable = data; %data should be the responses table
    end
    %get unique responses
    uniqueAnsws = unique(lower(string(dataTable.responses(dataTable.responses ~= ""))));
    %add them to list
    allSpellings = unique([allSpellings; uniqueAnsws]);
end
    
%% Showing the incorrect spellings to categorize what they are
%getting spellings that haven't been seen before
currSpellings = allSpellings;
for cs = numel(currSpellings):-1:1
    if ismember(currSpellings(cs), seenBefore)
        currSpellings(cs) = [];
    end
end
%skipping if there are no incorrect responses to code
if numel(currSpellings) > 0
    %add currSpellings to the seen before list so they don't come up again
    seenBefore = [seenBefore; currSpellings];
    %doing the thing
    for da = 1:numel(currSpellings)    
        %get one bad spelling at a time
        currSpell = char(currSpellings(da));    
        %making instructions for the window
        instructions = {'Select which target word the participant was', ...
                        'trying to type. Select "none" if they did not enter', ...
                        'a target word.', ...
                        '', ...
                        ['They entered: ', currSpell, ''], ...
                        '' ...
                        };     
        %setting up window to respond to the current word 
        list = [targets;'none'];
        [indx,tf] = listdlg('ListString', list, 'SelectionMode', 'single', 'Name', ['Spelling #', num2str(da), '/', num2str(numel(currSpellings)), ': '], 'PromptString', instructions, 'ListSize', [250 (numel(targets)+1)*15]);    
        %calling their word the fruit word that was selected as their actual answer
        target = string(list(indx));    
        %if they attempted a target word, add it to its fruit list
        if target ~= "none"
            targetSpellings.(target)(end+1) = currSpell;        
        end    
        %saving and moving to next word
        clear 'indx';    
        save(saveFileName, 'targetSpellings', 'allSpellings', 'seenBefore');
    end
    save(saveFileName, 'targetSpellings', 'allSpellings', 'seenBefore');
else
    disp('No new incorrect responses to grade :)');
end
% Function to get new correct/incorrect data
spellCheckingParticipants(filelist, targetSpellings, dataFileType);

end
















