%% Marking participants as correct/incorrect

function spellCheckingParticipants(filelist, targetSpellings, dataFileType)
%folder to save new data
newDataFolderName = 'spellCheckData';
newDataFolder = mkdir(newDataFolderName);
%read data
for s = 1:numel(filelist)
    %get table from csv or mat file
    if dataFileType == ".csv"
        %reading in csv
        dataTable = readtable(filelist(s).name);
    elseif dataFileType == ".mat"
        load(filelist(s).name);
        dataTable = data; %data should be the responses table
    end
    %going through trials and marking correct/incorrect
    for t = 1:numel(dataTable.responses)
        currResp = lower(string(dataTable.responses(t)));
        currTarg = lower(string(dataTable.targets(t)));
        if ismember(currResp, lower(targetSpellings.(currTarg)))
            dataTable.correct(t) = 1;
        else
            dataTable.correct(t) = 0;
        end
    end
    newDataFileName = ['.\' newDataFolderName '\' filelist(s).name(1:end-4) '.mat'];
    save(newDataFileName, 'dataTable');
end
end
